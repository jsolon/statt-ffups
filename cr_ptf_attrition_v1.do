/** cr_ptf_attrition_v1

Attrition fpr Post-Tx ffup
Attempting to reconcile log data with data being reported at St-att Meetings and Pivot table of logs.

Feb 23 2021
J Solon
*/

version 14

macro drop _all 
capture log close 
clear all 
drop _all 
set linesize 80

log using ~/Documents/0/statt/log/ffup.log, replace text 

set more off, permanently

cd ~/Documents/0/statt/data

* This imports the St-att log
import delim using "St-ATT_Updated Logs_Cebu_2019 - 9. Patient Post-Treatment Follow-Up .csv", delim(",") varn(1)

* Delete non-observations
* browse /* uncomment as needed*/
drop if registrationno ==.
drop if registrationno ==554 

* The source file has some mistakes which we correct as they should be "N" for enroled
*439	Compostela RHU	PIF	LB
*440	Compostela RHU	PIF	IM


list tb* enrolled* if registrationno ==439 | registrationno ==440
replace  enrolled* = "N" if registrationno ==439 | registrationno ==440
list tb* enrolled* if registrationno ==439 | registrationno ==440

* shortening and labelling var names 
rename posttxffupeligible ptf_eligible
rename posttxffupagreeyn ptf_agree
rename posttxffupicfobtaineddate ptf_icf
rename tbpatientid pid

la var ptf_a "Agreed"
la var ptf_eligible "Eligible for PTF"
la var ptf_icf "Obtained ICF"
la var pid "Patient ID"
la var enrolled "Enrolled"

d ptf* enrolled

* Tabulating frequencies of responses to relevant vars
local vars "pid enrolled ptf*"
mdesc `vars' 
tab1 `vars'
tab1 `vars', mis
	* 346 with pid, 207 with missing pid out of 553;  /* characterize refusals, check baseline paper */
	* 346 enrolled out of 553, 3 missing, 204 not enrolled /* confri if missing is not enrolled */
	* 245 eligible out of 346; 101 not eligible, 207 missing /* recode missing as not applicable as not enroled ;  characterize not eligible */ 
	* 186 agree out of 331, 145 out of 331 do not agree; 222 missing (total 553) ; as 207 are not enrolled, excess of 15 unaccounted among the 222 missing
	* 53 with ICF, 144, no ICF; 227 missing, rest with dates, total 553 /* denominator should be eligible and/or agree*/
	
	* Through mdesc and tab 1, for investigation:
		*the missings for enrolled (e) ptf_agree (222)  
		*and ptf_icf (227) 
		
* Missing  
local vars "pid enrolled ptf*"

	* Enrolled ==. >> 3
	list `vars' reg* if enrolled=="" /*these patients have no pid and checking tab 1 in log, were not enroled ; thus : recode as enrolled =="N"*/

	* pid == "" >> 207
	tab enrolled if pid=="" /* showing these are all unenrolled; pid be recode in Stata as "Not Enrolled" under pid */
	
	* ptf_eligible =="" >> 207
	tab ptf_e enrolled, mis /* all those not enrolled have missing eligibility data ; recode ptf_e as "Not Enrolled" if ptf_e==""*/
	
	* ptf_agree =="" >> 222
	bysort enrolled: tab ptf_e ptf_a, mis 
		/* 
		1. shows that  enrolled ==. and enrolled =="N" 
		2. 15 with enrolled=="Y" , ptf_a=="" >> if they are pending , probably code as pending IN LOG.  Empty cells have no meaning .*/
		* 222 = 207 who are not enrolled plus 15 who are eligible with 15 missing data for ptf_a
	
	* ptf_i ==. >> 227
	tab ptf_i, mis
		tab ptf_i enrolled, mis 
		tab ptf_i ptf_e, mis 
		tab ptf_i ptf_a, mis 
		tab ptf_e ptf_a, mis
		
		/* From tab....
		
		There are 227 with missing icf data (from tab ptf_i enrolled, mis)
		Cross tabulating enrolled and icf, 
			3 with missing ptf_i are also missing for enrolled.  Verified that they are not enrolled
			20 who are enrolled have missing ptf icf info. Unclear if they are eligible. See below
			346 who are enrolled, 144 have no ICF, and 53 have ICF, while the rest who are not missing have dates instead of Yes for ICF.
		
		Cross tabulating eligible and icf,
			207 missing for both; These are unenrolled
			101 not eligible; 100 with no ICF and 1 with ICF date
			245 are eligible; 53 have ICF, 44 with no ICF, 20 wiht missing ICF data, the remainder with dates thus should be added to with ICF
			
			* 	how many should be added to with ICF among those who are eligible and have icf dates */
			
				count if ptf_i!="N" & ptf_i!="Y" & ptf_i!="" /*129 with dates; thus 129 + 53 = 182  among eligible with ICF = 182*/
			* To describe individuals who are enrolled, eligible and with missing ICF info. 
			
			bysort ptf_a: tab ptf_e enrolled, mis if ptf_i=="" /* 15 with missing agree data; ptf_a="N" = 1 and four agreed
	
	
	
	*** verified code ends ***
	
	/* NOTES  and unverified code */
			missing for icf ,but enrolled ="Y" >>  
			1 with date, not eligible; thus, eligible with no ICF obtained. verified in log that this patient is dead ssee below.*/
			222 missing for both ner; 
		
		4 missing for icf ,but enrolled ="Y" >>; 
		1 with date did not agree - see below, this guy died
		1 with no ICF but agreed */
		count if ptf_i!="N" & ptf_i!="Y" & ptf_i!="" /*129*/
		
		list reg pid ptf* if ptf_i!="N" & ptf_i!="Y" & ptf_i!="" & ptf_e=="N" /* reg 88, pid 107-pif-016-tb-403 died , based on log*/
		
		
		
		list ptf_i pid if ptf_i!="N" & ptf_i!="Y" & ptf_i!="" /* all of those with dates have pids */
		list enrolled ptf_e ptf_a ptf_i pid if ptf_i=="" 
			/*Some of those with missing ptf_i data and are enrolled and eligible, have missing data for ptf_i
		/* Among 20 of those enrolled and eligible have missing pta_a data
			Pids int this source filed of those with ptf_i have pids ; so patient*/ 
	
	gen ptf_date_icf=ptf_icf /*keeps those dates*/
	replace ptf_d ="" if ptf_d =="N" | ptf_d =="Y" /* recodes y/n as missing date strings */
	
	replace ptf_icf="Y" if ptf_icf !="N" /* recodes all those with dates as Y for having ICF */
	
* Cross Tabs
	bysort enrolled: tab ptf_e ptf_i, mis row /* This shows those obtained consent among eligible ; 
		note discrepancy with st-att mgmt report as ff:
		Enrolled and ptf_e is missing but obtained ICF = 3
		 */
	
	bysort ptf_e: tab ptf_a ptf_i, mis row /* ths shows in the bysort ptf_e=="N", there is one who is not eligible, but agreed and we have an ICF seen also above */
		/*  in ptf_e=="Y" ; 
		Among those eligible , there is also another individual recorded as not agreed, but or which consent was obtained 
		15 with missign ptf_a (Agreed) but with with ICF
		1 who agreed but no ICF obtained and 185/186 fo those who agreed with have an ICF
		In the end, we have 
		*/
		

/*

area
eligible
approached
consent
with ODK data
%ODK/Consent

CEB
248 eligible in st-att report,  - 245 in stata and column summary in Sheets using countif() 
228 approached in st-att report (based on?); unclear how to recalculate in Stata as source in Log is unclear 
169 consented in st-att report - in Stata, cleaned as above ; with icf as dates or Y and eligilbe = 182
146 from ODK with data in st-att report and also now in sheets (unclear how this is merged in a replicable manner)
86.3

*/ 
Eligible for	Obtained	ICF
PTF	N	Y	Total
			
N	100	1	101 
	99.01	0.99	100.00 
			
Y	44	201	245 
	17.96	82.04	100.00 
			
Total	144	202	346 
	41.62	58.38	100.00 


