/** cr_ptf_eligibility.do
Version 0.1
Determines eligible patients from MK4
Based on Julius' code from ptx_eligible_cebu 
J Solon
Feb 25 2021
St-ATT Project for Sampa
- using Julius' code, but modifying approach 
- create a master eligiblity file from MK4 for all sites, instead of just Cebu
- this can be merged with logs of all sites so that data collected by nurses can be merged.
- uses csvconvert which requires all .csvs to be appended in one directory 
*/

version 14

macro drop _all 
capture log close 
clear all 
drop _all 
set linesize 80
set more off, permanently

* ALTERNATIVE PATH SCRIPT
	* local dir "~/Dropbox/TB_Nutrition/Data_Analysis/St-ATT/Datasets/" 
	* cd `dir'

* LOG
log using ~/Documents/0/statt/log/ptf_elig.log, replace text /* This is user-defined */

* PWD
cd ~/Documents/0/statt/data /* This is a user-defined project folder for data */

* NET INSTALLS - 
* net install csvconvert

* IMPORT LOG CSVs
csvconvert ~/Documents/0/statt/data/csvs, replace output_dir(~/Documents/0/statt/processed) output_file(ptf_logs)

* MISSINGS of v1 to followupfurn
missings dropvars registration_no - followupfurn

missings dropobs registration_no - followupfurn, force

* DROP OBS

drop if tbpatient=="" | tbpatient=="." | tbpatient =="Total" | tbpatient== "TOTAL"

* KEEP VARS
local vars "studysiteclinic tbpatientid patientinitials barangay posttxffupeligible posttxffupagreedyn posttxffupicfobtaineddate"
tab1 `vars'
keep `vars'

* SHORTEN VARS
rename tbpatientid pid
rename barangay brgy
rename posttxffupeligible ptf_eligible
rename posttxffupagreedyn ptf_agreed
rename posttxffupicfobtaineddate ptf_icf

la var pid "TB Patient ID"
la var brgy "Barangay"
la var ptf_eligible "Post-Tx: Eligible"
la var ptf_agreed "Post-Tx: Agreed"
la var ptf_icf "Post-Tx : ICF"

*CLEAN VARS	
 local vars "pid ptf_e ptf_a ptf_i"
 
 foreach var of varlist pid ptf_e ptf_a ptf_i {
 gen len_`var' = strlen(`var')
 }
 
 *CHECK WEIRD STRING LENGTHS - pid has 2 with 17 and 2 with 19
 local vars "len*" 
 tab1 `vars'
 
	* REPLACE PIDS WITH WRONG STRING LENGTH = WRONG IDS
	list pid if len_pid!=18 
		replace pid = "104-CDA-008-TB-004" if pid == "104-CDA-008TB-004"
		replace pid = "105-CDA-051-TB-119" if pid == "105- CDA-051-TB-119"
		replace pid = "111-CSB-002-TB-160" if pid == "111-CSB,-002-TB-160"
		replace pid = "111-CSB-006-TB-090" if pid == "111-CSB-006-TB-90"
	
	 gen len2_pid = strlen(pid)
	 tab len2*
	 
	 drop len*

* CHECK ptf variables
* ICF dates
tab ptf_i, mis
	gen ptf_d_icf =""
	replace ptf_d_icf =ptf_i if strlen(ptf_icf)>1
	replace ptf_i="Y" if ptf_i !="N" & ptf_i !="Y" & ptf_i !=""
la var ptf_d "Date ICF Obtained"


	/*** CHECK IMPORTS */
	
	
 * NOTE
 /*
 THESE Are the file names




 St-ATT_Updated Logs_Cebu_2019 - 9. Patient Post-Treatment Follow-Up .csv
 St-ATT_Updated Logs_Manila_2019 - 10. Post- Treatment FF-Up.csv  
 St-ATT_Updated Logs_Negros_2019 - 9. Post Tx-FF up.csv 

*/
 *cd ~/Documents/0/statt/data/csvs
 *sTATT_Updated St-ATT_Updated Logs_Cebu_2019 - 9. Patient Post-Treatment Follow-Up .csv
 *import delim using "St-ATT_Updated Logs_Negros_2019 - 9. Post Tx-FF up.csv", delim(",") clear
*/


	
/* Using this goes to the root directory then to the alternative path */
local dir "~/Dropbox/TB_Nutrition/Data_Analysis/St-ATT/Datasets/" 
cd `dir'
  * use "St-ATT_Dataset_BASELINE_Mk4.dta", clear

/* MERGE WITH MK4 from Dropbox*/
rename pid patient_id
merge m:m patient_id using "St-ATT_Dataset_BASELINE_Mk4.dta", generate(merge_1)

tab merge_1

/*
                merge_1 |      Freq.     Percent        Cum.
------------------------+-----------------------------------
        master only (1) |         28        3.02        3.02
         using only (2) |         25        2.69        5.71
            matched (3) |        875       94.29      100.00
------------------------+-----------------------------------
                  Total |        928      100.00
*/


list patient_id ptf* if merge_1==1 /*28 participants are in log and unmatched */


* merge m:m patient_id using "St-ATT_Dataset_BASELINE_Mk4.dta", generate

* CREATE eligibility FILE */

/* Recodes reason into eligibility */
label list reason
recode reason 	(4 5 = 1) ///
					(-99 0 1 2 3 6= 0) , ///
					generate (ptf_mk4_eligible) ///
				label(eligible)
la var ptf_mk4_eligible "Eligible for Post-Tx Ffup from MK4"
label define eligible 0 "Not Eligible" 1 "Eligible"
label values ptf_mk4_eligible eligible 

/* Keeps andor  Exports */
keep patient_id reason ptf* region healthcenter merge_1 

cd ~/Documents/0/statt/processed

export delimited using log_mk4.csv, delim(",") replace /* send to BEnjamin as query*/

keep if merge_1 !=1 /* keeps only mk4 patient ids */
export delim using log_mk4_900.csv, delim(",") replace

tab reason ptf_mk4*, mis
cd ~/Documents/0/statt/results

tab region ptf_mk4, mis row
bysort region: tab reason ptf_mk4, mis col

tab ptf_e region, mis /* Negros Occidental and Manila have no eligibiltiy data in logs */\tab ptf_e region, mis /* Negros Occidental and Manila have no eligibiltiy data in logs */
tab ptf_a region, mis /* Negros Occidental and Manila have no agree data in logs */
/*
\

*/
