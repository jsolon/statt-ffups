/** cr_master_ffups
Master for 
calculate ffup delays

Jun 23 2020
J Solon
*/

version 14

macro drop _all 
capture log close 
clear all 
drop _all 
set linesize 80

log using ~/Documents/0/statt/3_logs/ffup.log, replace text 

set more off, permanently

cd ~/Documents/0/statt/

/* This imports the log */

tempfile 1 2 3


import delim using "St-ATT_Updated Logs_Negros_2019 - Combined.csv", delim(",") clear
do cr_log_ffup_neg.do
save `1'

import delim using "St-ATT_Updated Logs_Cebu_2019 - Combined.csv", delim(",") clear
do cr_log_ffup.do
save `2'
/*
import delim using "St-ATT_Updated Logs_Manila_2019_Combined.csv", delim(",") clear
do cr_log_ffup.do
save `3'

*/

use `1', clear
append using `2', force

* append using `3', force


/* Inspect for errors */ 

sum between, detail

/* Outlier s - fix later */
count if between>300 & between !=.  /* 2019 of m_a2 should be 2020 */
count if between < -100 

gen omit =0 
replace omit =1 if between>300 
replace omit = 1 if between < -200
replace omit = 1 if between ==.


/* Add vars */
gen site = 1
replace site = 2 if town=="107" | town=="108" | town=="109" | town=="110" | town=="111"
*replace site = 3 if town=="107" | town=="108" | town=="109" | town=="110" | town=="111"

lab var site "Site"
lab define site 1"Negros Occidental" 2 "Cebu" 3 "Manila"
label values site site

/* 103, 104, 105 , 106 and 113 = negros
107-108 109 110 111 = cebu
*/


/* Graph */

lab var w_e2 "Week"

gr bar (median) between if omit==0 & site==1, over (w_e2, label(labsize(tiny))) ytitle("Delay (days)") b1title("Week") ti("Median delay in follow-up")
gr save 1, replace

gr bar (median) between if omit==0 & site==2, over (w_e2, label(labsize(tiny))) ytitle("Delay (days)") b1title("Week") ti("Median delay in follow-up")
gr save 2, replace

gr combine 1.gph 2.gph

gr bar (median) between if omit==0, over (w_e2, label(labsize(tiny))) ytitle("Delay (days)") b1title("Week") ti("Median delay in follow-up")
gr save 2, replace
gr bar (median) between if omit==0, over (w_e2, label(labsize(tiny))) ytitle("Delay (days)") b1title("Week") ti("Median delay in follow-up")





gen bet7=0

replace bet7=1 if between <7 & between !=. & between >  - 1000

gr bar bet7, over(w_e2)

gr hbar bet7, over(w_e2)

/* UNCORRECTED ERRORS 
Date is written as 0202 if between < -1000
2019 written as 2020 if f between>100 & between !=. 
*/





outsheet  using log_cdaffup.csv, comma replace
