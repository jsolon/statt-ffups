/** cr_statt_ffups
calculate ffup delays
Note- not possible as there are non-date text in vars.
This renames vars and reshapes vars to long
Jun 23 2020
J Solon
*/

drop preventsort

/* This prepares the variable names for reshaping */
rename month1expected m_e1
rename month1actual m_a1

rename month2endofipfordsexpected m_e2
rename month2endofipfordsactual m_a2

rename month3expected m_e3
rename month3actual m_a3

rename month4midcpfordsendofipfordrexpe m_e4
rename month4midcpfordsendofipfordractu m_a4

rename month5expected m_e5
rename month5actual m_a5

rename month6endofcpfordsexpected m_e6
rename month6endofcpfordsactual m_a6

rename month7midcpfordrexpected m_e7
rename month7midcpfordractual m_a7

rename month8expected m_e8
rename month8actual m_a8

rename month9endofcpfordrexpected m_e9
rename month9endofcpfordractual m_a9

rename month10expected m_e10
rename month10actual m_a10

/* replaces m_e10 to string  to avoid error */
tostring m_e10, replace


/* To identify nurse and site */

gen str town = regexs(0) if regexm(tbpatientid,"^[0-9][0-9][0-9]")
gen str rn2 = regexs(0) if regexm(tbpatientid,"^[0-9][0-9][0-9]-[a-zA-Z][a-zA-Z][a-zA-Z]")
egen rn=ends(rn2), punct("-") tail
drop rn2

gen site = 1
replace site = 2 if town=="107" | town=="108" | town=="109" | town=="110" | town=="111"
*replace site = 3 if town=="107" | town=="108" | town=="109" | town=="110" | town=="111"

lab var site "Site"
lab define site 1"Negros Occidental" 2 "Cebu" 3 "Manila"
label values site site

/* Reshapes dataset from wide to long using keeping aats as m_e (expected) and m_a(actual), and generating fmon as the monthly visit var*/

reshape long m_e m_a, i(tbpatientid) j(fmon) string

/* this makes fmon a number so we can sort it the month visit without and avoid 10 coming after 1 - there must be a better way to do this */
destring fmon, replace

/* Drop Month 11 onwards */
drop month11expected - month24actual
order tbpatientid fmon m_e m_a
sort tbpatientid fmon

/*
Fixes text in var m_a as this contains other non-date texts 
We generate a new date variable for actual date 
We replace m_a2 with the date extracted from m_a removing any other characters )
Then we generate a date variable as m_a3
*/

* generate m_a2 = m_a by site
*Negros
gen str m_a2 = regexs(0) if regexm(m_a,".[0-9]-[a-zA-Z][a-zA-Z][a-zA-Z]-[0-9][0-9][0-9][0-9]")
gen m_a3=date(m_a2, "DMY")
format m_a3 %td

*Cebu/*
gen str m_a2_c = regexs(0) if site==2 & regexm(m_a,".[0-9][a-zA-Z][a-zA-Z][a-zA-Z][0-9][0-9][0-9][0-9]")  /* for date ddmmmyyyy*/
gen m_a2_c1 = date(m_a,"DMY")
format m_a2_c1 %td

gen m_a2_c2 = regexs(0) if site==2 & regexm(m_a,".[0-9]/[a-zA-Z]/[0-9][0-9]/[0-9][0-9]") /* for date mm/dd/yy*/

gen str m_a2_c1 = regexs(0) if site==2 & regexm(m_a,".[0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]") /* for date mm/dd/yyyy*/
gen str m_a2_c2 = regexs(0) if site==2 & regexm(m_a,".[0-9]/[0-9][0-9]/[0-9][9]$") /* for date mm/dd/yy*/
gen str m_a2_c3 = regexs(0) if site==2 & regexm(m_a,".[0-9]/[0-9][0-9]/[0-9][0]$") /* for date mm/dd/yy*/
gen str m_a2_c4 = regexs(0) if site==2 & regexm(m_a,".[0-9]/[0-9][0-9]/[0-9][8]$") /* for date mm/dd/yy*/
gen str m_a2_c5 = regexs(0) if site==2 & regexm(m_a,".[0-9]/[0-9][0-9]/[20][19]$") /* for date mm/dd/yy*/
gen str m_a2_c6 = regexs(0) if site==2 & regexm(m_a,".[0-9]/[0-9][0-9]/[20][20]$") /* for date mm/dd/yy*/



gen str m_a2_c3 = regexs(0) if site==2 & regexm(m_a,"[a-zA-Z][a-zA-Z][a-zA-Z][a-zA-Z] [0-9][0-9],[0-9][0-9] [0-9][0-9]") /* for date mm/dd/yy*/

gen m_a3=date(m_a2_c, "DMY")
format m_a3 %td
*/
/* date formats 
mm/dd/yy
mm/dd/yyyy
mmmm dd,yyyy
*/



/* We need to convert m_e expeted date to a date variable */

gen m_e2 = date(m_e, "DMY")
format m_e2 %td

/* Genreate days between expected and actual */
gen between = m_a3-m_e2

/* To graph by week, lets generate a weekly date */

gen w_e2 = week(m_e2)




* count if regexm(tbpatientid,"[0-9][0-9][0-9]-[a-zA-Z][a-zA-Z][a-zA-Z]-[0-9][0-9][0-9]-[a-zA-Z][a-zA-Z]-[0-9][0-9][0-9]")

